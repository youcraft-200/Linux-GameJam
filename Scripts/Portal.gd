extends Area2D

export (String) var NextScene 
func _on_Portal_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene(NextScene)
