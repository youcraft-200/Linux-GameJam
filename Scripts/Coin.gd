extends Node

func _ready():
	$AnimationPlayer.play("Idle")

func _on_Coin_body_entered(body):
	if body.name == "Player":
		get_parent().remove_child(self)
