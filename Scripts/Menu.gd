extends Control


func _on_PlayButton_pressed():
	get_tree().change_scene("res://Scenes/Level1Tutorial.tscn")


func _on_ButtonMainMenu_pressed():
	get_tree().change_scene("res://Scenes/Menu.tscn")


func _on_DevButton_pressed():
	OS.shell_open("https://youcraft200.itch.io/")
