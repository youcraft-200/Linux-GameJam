extends RigidBody2D

export var speed = 400

var touch_ground = true

func _physics_process(delta):
	rotation_degrees = 0
	move()

func move():
	var my_vert_vel = get_linear_velocity().y
	
	if Input.is_key_pressed(KEY_D):
		set_linear_velocity(Vector2(1 * speed, my_vert_vel))
	elif Input.is_key_pressed(KEY_A):
		set_linear_velocity(Vector2(-1 * speed, my_vert_vel))
	else: 
		set_linear_velocity(Vector2(0, my_vert_vel))
		
	if Input.is_action_just_pressed("Jump"):
		if touch_ground == true:
			if gravity_scale == 10:
				gravity_scale = -10
				$JumpSound.playing = true
			elif gravity_scale == -10:
				gravity_scale = 10
				$JumpSound.playing = true

func _on_HitBox_body_entered(touched):
	if touched.get_name() == "TileMap":
		touch_ground = true


func _on_HitBox_body_exited(touched):
	if touched.get_name() == "TileMap":
		touch_ground = false


func _on_SecondHitBox_body_entered(HitBox):
	if HitBox.get_name() == "Liquids":
		get_tree().reload_current_scene()
